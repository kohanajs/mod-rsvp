const {ORM} = require('kohanajs');

class LeadLogin extends ORM{
  lead_id = null;
  ip = null;

  static joinTablePrefix = 'lead_login';
  static tableName = 'lead_logins';

  static fields = new Map([
    ["ip", "String"]
  ]);
  static belongsTo = new Map([
    ["lead_id", "Lead"]
  ]);
}

module.exports = LeadLogin;
