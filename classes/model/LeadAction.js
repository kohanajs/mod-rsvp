const {ORM} = require('kohanajs');

class LeadAction extends ORM{
  name = null;
  ip = null;
  payload = null;
  belongs_to = null;

  static joinTablePrefix = 'lead_action';
  static tableName = 'lead_actions';

  static fields = new Map([
    ["name", "String!"],
    ["ip", "String!"],
    ["payload", "String"],
    ["belongs_to", "Leads"]
  ]);
}

module.exports = LeadAction;
