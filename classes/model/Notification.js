const {ORM} = require('kohanajs');

class Notification extends ORM{
  lead_id = null;
  name = null;
  status = null;
  message_id = null;

  static joinTablePrefix = 'notification';
  static tableName = 'notifications';

  static fields = new Map([
    ["name", "String!"],
    ["status", "String"],
    ["message_id", "String"]
  ]);
  static belongsTo = new Map([
    ["lead_id", "Lead"]
  ]);
}

module.exports = Notification;
