const {ORM} = require('kohanajs');

class Lead extends ORM{
  lead_state_id = 3;
  lead_type_id = 1;
  language = "zh-hant";
  name = null;
  title = null;
  last_name = null;
  chinese_name = null;
  company_name = null;
  company_chinese_name = null;
  company_position = null;
  company_phone_area_code = null;
  company_phone = null;
  website = null;
  address_1 = null;
  postal_code = null;
  country = null;
  email = null;
  phone_area_code = null;
  phone = null;
  consent = null;
  consent_gdpr = null;
  rsvp_code = null;
  utm_source = null;
  utm_medium = null;
  utm_campaign = null;
  utm_term = null;
  utm_content = null;

  static joinTablePrefix = 'lead';
  static tableName = 'leads';

  static fields = new Map([
    ["language", "String!"],
    ["name", "String!"],
    ["title", "String!"],
    ["last_name", "String"],
    ["chinese_name", "String"],
    ["company_name", "String"],
    ["company_chinese_name", "String"],
    ["company_position", "String"],
    ["company_phone_area_code", "String"],
    ["company_phone", "String"],
    ["website", "String"],
    ["address_1", "String"],
    ["postal_code", "String"],
    ["country", "String"],
    ["email", "String"],
    ["phone_area_code", "String"],
    ["phone", "String"],
    ["consent", "Boolean"],
    ["consent_gdpr", "Boolean"],
    ["rsvp_code", "String"],
    ["utm_source", "String"],
    ["utm_medium", "String"],
    ["utm_campaign", "String"],
    ["utm_term", "String"],
    ["utm_content", "String"]
  ]);
  static belongsTo = new Map([
    ["lead_state_id", "LeadState"],
    ["lead_type_id", "LeadType"]
  ]);
  static hasMany = [
    ["lead_id", "LeadLogin"],
    ["lead_id", "Visit"],
    ["lead_id", "Notification"]
  ];
  static belongsToMany = new Set([
    "Content"
  ]);
}

module.exports = Lead;
