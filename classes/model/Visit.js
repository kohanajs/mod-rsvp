const {ORM} = require('kohanajs');

class Visit extends ORM{
  lead_id = null;
  content_id = null;
  ip = null;

  static joinTablePrefix = 'visit';
  static tableName = 'visits';

  static fields = new Map([
    ["ip", "String"]
  ]);
  static belongsTo = new Map([
    ["lead_id", "Lead"],
    ["content_id", "Content"]
  ]);
}

module.exports = Visit;
