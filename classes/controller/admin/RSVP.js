const {ControllerAdmin} = require('@kohanajs/mod-admin');
const {KohanaJS, ORM, ControllerMixinDatabase} = require('kohanajs');
const Lead = ORM.require('Lead');
const HelperRSVP = require('../../helper/RSVP');

class ControllerAdminRSVP extends ControllerAdmin {
  constructor(request) {
    super(request, Lead, {
      roles: new Set(['staff', 'moderator']),
      databases: new Map([
        ['guest', KohanaJS.config.setup.databaseFolder+'/guest.sqlite'],
        ['mail', KohanaJS.config.setup.databaseFolder+'/mail.sqlite']
      ]),
      templates: new Map([
        ['index', 'templates/admin/guests/index'],
        ['read', 'templates/admin/guests/read'],
        ['edit', 'templates/admin/guests/read'],
      ]),
      database: 'guest',
      pagesize: 50,
    });
  }

  async action_index() {}

  async action_send_email(){
    const databases = this.state.get(ControllerMixinDatabase.DATABASES);
    const database = databases.get('guest');
    const instance = await ORM.factory(Lead, this.request.params.id, {database});

    const helperRegister = new HelperRSVP(
      databases.get('guest'),
      databases.get('mail'),
      this.clientIP,
      this.request.hostname
    )

    await helperRegister.sendRSVP(instance, KohanaJS.config.edm);
    instance.rsvp_code = "111.111.111";
    await instance.write();

    await this.redirect('/admin/rsvp/');
  }

}

module.exports = ControllerAdminRSVP;
