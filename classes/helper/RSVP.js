const {ORM, KohanaJS} = require('kohanajs');
const {ORMAdapterSQLite} = require("@kohanajs/mod-database-adapter-better-sqlite3");
const {HelperMail} = require('@kohanajs/mod-mail');
const {MailAdapterAWS} = require('@kohanajs/mod-mail-adapter-aws');

const Notification = ORM.require('Notification')

class HelperRSVP {
  constructor(leadDB, mailDB, clientIP, landing) {
    this.leadDB = leadDB;
    this.helperMail = new HelperMail(landing, clientIP, {
      database: mailDB,
      ormAdapter : ORMAdapterSQLite,
      adapter : MailAdapterAWS
    });
    this.clientIP = clientIP;
    this.landing = landing;
  }

  async sendReminder(instance, config){
    const language = instance.language;
    const userEmail = instance.email;
    const sender  = config.mail.reminder.sender;
    const subject = config.mail.reminder.subject.get(language);
    const text    = config.mail.reminder.text.get(language);
    const html    = config.mail.reminder.html.get(language);

    const title = new Map([
      ["en", new Map([["mr", "Mr."], ["mrs", "Mrs."], ["ms", "Ms."]])],
      ["zh-hans", new Map([["mr", "先生"], ["mrs", "太太"], ["ms", "女士"]])],
      ["zh-hant", new Map([["mr", "先生"], ["mrs", "太太"], ["ms", "女士"]])],
    ])

    const tokens = Object.assign(
      {
        domain: this.landing,
        language: language,
      },
      instance,
      {
        email: userEmail.replaceAll('.', '<span>.</span>'),
        title: title.get(language).get(instance.title.toLowerCase()),
      });

    const result = await this.helperMail.send(subject, text, sender, userEmail, {
      bcc: config.mail.bcc,
      htmlTemplate: html,
      tokens
    });

    await this.addNotification('REMINDER', result, instance.id);
  }

  async sendRSVP(instance, config){
    const language = instance.language;
    const userEmail = instance.email;
    const sender  = config.mail.rsvp.sender;
    const subject = config.mail.rsvp.subject.get(language);
    const text    = config.mail.rsvp.text.get(language);
    const html    = config.mail.rsvp.html.get(language);
    const attachments = config.mail.rsvp.attachments.get(language);

    const title = new Map([
      ["en", new Map([["mr", "Mr."], ["mrs", "Mrs."], ["ms", "Ms."]])],
      ["zh-hans", new Map([["mr", "先生"], ["mrs", "太太"], ["ms", "女士"]])],
      ["zh-hant", new Map([["mr", "先生"], ["mrs", "太太"], ["ms", "女士"]])],
    ])

    const tokens = Object.assign(
      {
        domain: this.landing,
        language: language,
      },
      instance,
      {
        email: userEmail.replaceAll('.', '<span>.</span>'),
        title: title.get(language).get(instance.title.toLowerCase()),
      })

    const result = await this.helperMail.send(subject, text, sender, userEmail, {
      bcc: config.mail.bcc,
      htmlTemplate: html,
      tokens,
      attachments
    });

    await this.addNotification('RSVP', result, instance.id);
  }

  async addNotification(name, result, leadID){
    const note = ORM.create(Notification, {database: this.leadDB});
    note.name = name;
    note.status = JSON.stringify(result);
    if(result.id) note.message_id = result.id.replace('<', '').replace('>', '');
    note.lead_id = leadID;
    await note.write();
  }
}

module.exports = HelperRSVP;
