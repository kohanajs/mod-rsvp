const path = require('path');
const {build} = require('kohanajs-start');

build(
  `${__dirname}/lead.graphql`,
  `${__dirname}/guest.js`,
  `${__dirname}/exports/guest.sql`,
  `${__dirname}/default/db/guest.sqlite`,
  path.normalize(`${__dirname}/classes/guest/model`),
)

build(
  `${__dirname}/mail.graphql`,
  ``,
  `${__dirname}/exports/mail.sql`,
  `${__dirname}/default/db/mail.sqlite`,
  path.normalize(`${__dirname}/classes/mail/model`),
)