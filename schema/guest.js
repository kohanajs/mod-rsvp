const list_zh_hant = [
  ["","hello@example.com"],
]

const guests_zh_hant = list_zh_hant.map((x, i) => ({
  id: 1000+i,
  language: "zh_hant",
  title: "",
  name: x[0],
  last_name: "",
  email: x[1]
}));

const list_en = [
  ["","hello-en@example.com"],
]

const guests_en = list_en.map((x, i) => ({
  id: 2000+i,
  language: "en",
  title: "",
  name: x[0],
  last_name: "",
  email: x[1]
}));

module.exports = new Map([
  ['LeadStates', [
    {id: 1, name: 'pending'},
    {id: 2, name: 'reject'},
    {id: 3, name: 'active'},
    {id: 4, name: 'suspend'},
  ]],
  ['LeadTypes', [
    {id: 1, name: 'guest'},
  ]],
  ['Leads', [
    {id:1, language:"zh-hant", title: "", name: "", last_name: "", email: "start@kohanajs.com"},
    {id:2, language:"en", title: "", name: "", last_name: "", email: "start-en@kohanajs.com"},
    ...guests_zh_hant,
    ...guests_en
  ]],
]);