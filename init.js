const {KohanaJS} = require('kohanajs')
KohanaJS.initConfig(new Map([
  ['rsvp', require('./config/rsvp')],
]));