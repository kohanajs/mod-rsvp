# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.2](https://gitlab.com/kohanajs/mod-rsvp/compare/v1.0.1...v1.0.2) (2022-03-06)


### Bug Fixes

* missing model ([b512ce0](https://gitlab.com/kohanajs/mod-rsvp/commit/b512ce0b91250f835e19955c904f43b7841dd742))

### 1.0.1 (2022-03-06)
