const path = require('path');
const {KohanaJS} = require('kohanajs');

function getAdmin(){
  return "admin@cowise.co";
}

function getBCC(){
  return "colin@digi3studio.com"
}

const templatePath = KohanaJS.EXE_PATH + '/../public/media/edm'

module.exports ={
  mail: {
    admin: getAdmin(),
    bcc: getBCC(),
    sender: 'Hong Kong Federation of Women <hkfwwomensday@hkfw.org>',
    templatePath,

    rsvp:{
      sender: 'Hong Kong Federation of Women <hkfwwomensday@hkfw.org>',
      subject: new Map([
        ['en', "Happy International Women's Day to all of you!"],
        ['zh-hans', '香港婦協祝您三八國際婦女節快樂!'],
        ['zh-hant', '香港婦協祝您三八國際婦女節快樂!']
      ]),
      text: new Map([
        ['en', 'Happy International Women’s Day to all of you!\n\nInternational Women’s Day is a time to celebrate the many achievements women have made in all fields and aspects, and a day to commemorate the many strides made over the years in establishing gender equality.\n\nMore importantly, it is also an opportunity for us to build on our dreams for a better and more diverse society for our future generations.  During the pandemic, many women have taken on leading and professional roles, and have dedicated their time and effort, in the fight against COVID-19. At the same time, they have continued their parental duties, truly demonstrating how a woman can have both a successful career and motherhood.\n\nMarch 8th marks the all-important International Women’s Day. Together with our corporate members, Hong Kong Federation of Women (HKFW) sends our warmest blessings through this short video and sincerely wishes everyone a happy International Women’s Day!\n\nHKFW is truly grateful for your continued support, and looks forward to exploring more possible cooperation between women in the future.\n\nWith warmest regards,\nHong Kong Federation of Women'],
        ['zh-hans', '祝大家國際婦女節快樂！\n\n國際婦女節是慶祝婦女在不同領域上取得的成就及貢獻，也是紀念多年來在建立兩性平等方面邁進向前的日子。更重要的是，國際婦女節讓我們延續夢想，繼續為下一代打造一個更美好、更多元化的社會。\n\n在疫情期間，許多女性擔當了領導和專業角色，為抗擊 COVID-19 付出了時間和精力。同時，她們也繼續母親的職責，真實地展示女士能同時擁有成功事業和執行母親天職。\n\n3 月 8 日記錄著尤為重要的國際婦女節。香港各界婦女聯合協進會（香港婦協）與我們多個團體會員一起，通過這短片，為大家送上我們最熱烈的祝福，並衷心祝願大家國際婦女節快樂！\n\n香港婦協非常感謝您一直以來的支持，並期待在未來與您繼續探索更多婦女活動的合作。\n\nhttps://www.hkfwevent.org/'],
        ['zh-hant', '祝大家國際婦女節快樂！\n\n國際婦女節是慶祝婦女在不同領域上取得的成就及貢獻，也是紀念多年來在建立兩性平等方面邁進向前的日子。更重要的是，國際婦女節讓我們延續夢想，繼續為下一代打造一個更美好、更多元化的社會。\n\n在疫情期間，許多女性擔當了領導和專業角色，為抗擊 COVID-19 付出了時間和精力。同時，她們也繼續母親的職責，真實地展示女士能同時擁有成功事業和執行母親天職。\n\n3 月 8 日記錄著尤為重要的國際婦女節。香港各界婦女聯合協進會（香港婦協）與我們多個團體會員一起，通過這短片，為大家送上我們最熱烈的祝福，並衷心祝願大家國際婦女節快樂！\n\n香港婦協非常感謝您一直以來的支持，並期待在未來與您繼續探索更多婦女活動的合作。\n\nhttps://www.hkfwevent.org/']
      ]),
      html: new Map([
        ['en', 'en/rsvp.html'],
        ['zh-hans', 'zh-hans/rsvp.html'],
        ['zh-hant', 'zh-hant/rsvp.html']
      ]),
      attachments:new Map([
        ['en', [{
          filename: 'footer.jpg',
          path: path.normalize(templatePath + '/footer_eng.jpg'),
          cid: 'footer'
        }]],
        ['zh-hans', [{
          filename: 'footer.jpg',
          path: path.normalize(templatePath + '/footer_chi.jpg'),
          cid: 'footer'
        }]],
        ['zh-hant', [{
          filename: 'footer.jpg',
          path: path.normalize(templatePath + '/footer_chi.jpg'),
          cid: 'footer'
        }]],
      ]),
    },

    reminder: {
      sender: 'Hong Kong Federation of Women <hkfwwomensday@hkfw.org>',
      subject: new Map([
        ['en', 'Pansy Ho invites you to second edition of the Women Power Forum (Oct 23)'],
        ['zh-hans', '香港婦協祝您三八國際婦女節快樂!'],
        ['zh-hant', '香港婦協祝您三八國際婦女節快樂!']
      ]),
      text: new Map([
        ['en', 'Pansy Ho invites you to second edition of the Women Power Forum (Oct 23)'],
        ['zh-hans', '祝大家國際婦女節快樂！\n\n國際婦女節是慶祝婦女在不同領域上取得的成就及貢獻，也是紀念多年來在建立兩性平等方面邁進向前的日子。更重要的是，國際婦女節讓我們延續夢想，繼續為下一代打造一個更美好、更多元化的社會。\n\n在疫情期間，許多女性擔當了領導和專業角色，為抗擊 COVID-19 付出了時間和精力。同時，她們也繼續母親的職責，真實地展示女士能同時擁有成功事業和執行母親天職。\n\n3 月 8 日記錄著尤為重要的國際婦女節。香港各界婦女聯合協進會（香港婦協）與我們多個團體會員一起，通過這短片，為大家送上我們最熱烈的祝福，並衷心祝願大家國際婦女節快樂！\n\n香港婦協非常感謝您一直以來的支持，並期待在未來與您繼續探索更多婦女活動的合作。\n\nhttps://www.hkfwevent.org/'],
        ['zh-hant', '祝大家國際婦女節快樂！\n\n國際婦女節是慶祝婦女在不同領域上取得的成就及貢獻，也是紀念多年來在建立兩性平等方面邁進向前的日子。更重要的是，國際婦女節讓我們延續夢想，繼續為下一代打造一個更美好、更多元化的社會。\n\n在疫情期間，許多女性擔當了領導和專業角色，為抗擊 COVID-19 付出了時間和精力。同時，她們也繼續母親的職責，真實地展示女士能同時擁有成功事業和執行母親天職。\n\n3 月 8 日記錄著尤為重要的國際婦女節。香港各界婦女聯合協進會（香港婦協）與我們多個團體會員一起，通過這短片，為大家送上我們最熱烈的祝福，並衷心祝願大家國際婦女節快樂！\n\n香港婦協非常感謝您一直以來的支持，並期待在未來與您繼續探索更多婦女活動的合作。\n\nhttps://www.hkfwevent.org/']
      ]),
      html: new Map([
        ['en', 'en/reminder.html'],
        ['zh-hans', 'zh-hans/reminder.html'],
        ['zh-hant', 'zh-hant/reminder.html']
      ]),
      attachments:new Map([
        ['en', [{
          filename: 'footer.jpg',
          path: path.normalize(__dirname + '/footer_eng.jpg'),
          cid: 'footer'
        }]],
        ['zh-hans', [{
          filename: 'footer.jpg',
          path: path.normalize(__dirname + '/footer_chi.jpg'),
          cid: 'footer'
        }]],
        ['zh-hans', [{
          filename: 'footer.jpg',
          path: path.normalize(__dirname + '/footer_chi.jpg'),
          cid: 'footer'
        }]],
      ]),
    },
  },
}