const {KohanaJS} = require('kohanajs');

module.exports = {
  dbMail: `${KohanaJS.EXE_PATH}/../database/mail.sqlite`,
  dbWebhook: `${KohanaJS.EXE_PATH}/../database/webhook.sqlite`,

  aws: {
    credentialsPath : KohanaJS.APP_PATH + '/config/credentials',
    profile              : 'default',
    region               : 'ap-southeast-1',
    configurationSetName : "occasionspr",
    dynamoDB             : "SES_OCCA",
    project              : `hkfw-${KohanaJS.ENV}`,
  },
};
