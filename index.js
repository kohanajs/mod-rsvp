require('kohanajs').addNodeModule(__dirname);
const { RouteList } = require('@kohanajs/mod-route');

const ControllerRSVP = require('./classes/controller/admin/RSVP');
const HelperRSVP = require('./classes/helper/RSVP');

module.exports = {
  ControllerRSVP,
  HelperRSVP,
  routes : {
    apply: ()=>{
      RouteList.add('/admin/rsvp', 'controller/admin/RSVP');
      RouteList.add('/admin/rsvp/send/:id', 'controller/admin/RSVP', 'send_email');
    }
  }
};
